import React, { useState } from 'react';
//import logo from './micheal.svg';
import test from './test.wav';
import './App.css';
import { generate  } from './utils/words';
import useKeyPress from './hooks/useKeyPress';
import { currentTime } from './utils/time';
import listReactFiles from 'list-react-files';

const initialWords = generate();

function App() {
    const [leftPadding, setLeftPadding] = useState(
        new Array(20).fill(' ').join(''),
    );
    const [outgoingChars, setOutgoingChars] = useState('');
    const [currentChar, setCurrentChar] = useState(initialWords.charAt(0));
    const [incomingChars, setIncomingChars] = useState(initialWords.substr(1));
    const [startTime, setStartTime] = useState();
    const [wordCount, setWordCount] = useState(0);
    const [wpm, setWpm] = useState(0);
    const [accuracy, setAccuracy] = useState(0);
    const [typedChars, setTypedChars] = useState('');
    const [logo, setLogo] = useState('micheal.svg');
    
    useKeyPress(key => {
        let updatedOutgoingChars = outgoingChars;
        let updatedIncomingChars = incomingChars;
        if (!startTime) {
            setStartTime(currentTime());
        }

        if (incomingChars.charAt(0) === ' ') {
            setWordCount(wordCount + 1);
            const durationInMinutes = (currentTime() - startTime) / 60000.0;
            setWpm(((wordCount + 1) / durationInMinutes).toFixed(2));
        }

        if (key === currentChar) {
            if (leftPadding.length > 0) {
                setLeftPadding(leftPadding.substring(1));
            }
            updatedOutgoingChars += currentChar;
            setOutgoingChars(updatedOutgoingChars);

            setCurrentChar(incomingChars.charAt(0));

            updatedIncomingChars = incomingChars.substring(1);
            if (updatedIncomingChars.split(' ').length < 10) {
                updatedIncomingChars +=' ' + generate();
            }
            setIncomingChars(updatedIncomingChars);
        } else {
            let clip = play_sound();
            setLogo("micheal_angry.png");
            play(clip).then(function() {
                setLogo("micheal.svg");
            });
        }

        const updatedTypedChars = typedChars + key;
        setTypedChars(updatedTypedChars);

        setAccuracy(
            ((updatedOutgoingChars.length * 100) / updatedTypedChars.length).toFixed(
                2,
            ),
        );
    });

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p></p>
        <p className="Character">
            <span className="Character-out">
                {(leftPadding + outgoingChars).slice(-20)}
            </span>
            <span className="Character-current">{currentChar}</span>
            <span>{incomingChars.substr(0, 20)}</span>
        </p>
        <h3>
            WPM: {wpm} | ACC: {accuracy}%
        </h3>
      </header>
    </div>
  );
}


function play_sound() {
    let clips = [
        'dumbass.wav',
        'fuck_off.wav',
        'fuck_you.wav',
        'fuck_you2.wav',
        'fuck_you3.wav',
        'fuck_you4.wav',
        'fucker.wav',
        'fucker2.wav',
        'go_fuck_your_self.wav',
        'go_fuck_your_self2.wav',
        'little_shit.wav',
        'piece_of_shit.wav',
        'piece_of_shit2.wav',
        'piece_of_shit3.wav',
        'stupid_ass.wav',
        'thats_a_lot_of_shit.wav',
        'you_just_got_memed.wav',
    ];

    return "insulting_clips/" + clips[Math.floor(Math.random() * clips.length)];
}

function play(url) {
     return new Promise(function(resolve, reject) {
         var audio = new Audio(url);                 
         audio.preload = "auto";
         audio.autoplay = true;
         audio.onended = resolve;             
     });
}

export default App;
